﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Nomezeiro_2.Controls
{
    public class ChromeButton : Button 
    {
        static ChromeButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ChromeButton), new FrameworkPropertyMetadata(typeof(ChromeButton)));
        }
        public PackIconKind DefaultIcon
        {
            get { return (PackIconKind)GetValue(DefaultIconProperty); }
            set { SetValue(DefaultIconProperty, value); }
        }
        public PackIconKind HoverIcon
        {
            get { return (PackIconKind)GetValue(HoverIconProperty); }
            set { SetValue(HoverIconProperty, value); }
        }
        public static readonly DependencyProperty DefaultIconProperty = DependencyProperty.Register("DefaultIcon", typeof(PackIconKind), typeof(ChromeButton), null);
        public static readonly DependencyProperty HoverIconProperty = DependencyProperty.Register("HoverIcon", typeof(PackIconKind), typeof(ChromeButton), null);
    }
}
