﻿using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nomezeiro_2.Model;
using System.Threading;

namespace Nomezeiro_2.API
{
    public class NomezeiroAPI
    {
        const string BaseUrl = "https://nomezeiro.cotti.moe";

        private static string token = string.Empty;
        public static User CurrentUser { get; private set; }

        private static readonly Lazy<NomezeiroAPI> lazy = new Lazy<NomezeiroAPI>(() => new NomezeiroAPI());

        public static NomezeiroAPI Instance { get { return lazy.Value;} }

        private NomezeiroAPI()
        {
        }

        public async Task<bool> Initialize(string user, string pass)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseUrl);
            var request = new RestRequest(Method.POST);
            var cancelToken = new CancellationTokenSource();

            request.RequestFormat = DataFormat.Json;
            request.Resource = "auth";
            request.Parameters.Clear();
            request.AddJsonBody(new { username = user, password = pass });
            IRestResponse res = await client.ExecuteTaskAsync(request, cancelToken.Token);
            Debug.WriteLine(res.Content);
            var des = new JsonDeserializer().Deserialize<TokenUser>(res);
            var err = new JsonDeserializer().Deserialize<ErrorResponse>(res);
            if (err.Code != 0)
            {
                Debug.WriteLine($"Error: {err.Error}");
                return false;
            }
            if (res.ErrorException != null)
            {
                Debug.WriteLine($"error: {res.ErrorMessage}");
                return false;
            }
            token = des.Token;
            CurrentUser = des.User;
            return true;
        }

        public T Execute<T>(RestRequest request) where T: new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri(BaseUrl);
            client.Authenticator = new JwtAuthenticator(token);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute<T>(request);
            var err = new JsonDeserializer().Deserialize<ErrorResponse>(response);
            if (err.Code != 0)
                Debug.WriteLine($"Error: {err.Error}");
            if (response.ErrorException != null)
            {
                Debug.WriteLine($"error: {response.ErrorMessage}");
            }

            return response.Data;
        }

        public T Get<T>(NomezeiroModel m) where T: new()
        {
            var request = new RestRequest(Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.Resource = m.Resource;
            request.Parameters.Clear();

            return Execute<T>(request);
        }

        public T Post<T>(NomezeiroModel m) where T : new()
        {
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.Resource = m.Resource;
            request.Parameters.Clear();
            request.AddJsonBody(m.JsonBody);
            return Execute<T>(request);
        }
        public T Put<T>(NomezeiroModel m) where T : new()
        {
            var request = new RestRequest(Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.Resource = m.Resource;
            request.Parameters.Clear();
            request.AddJsonBody(m.JsonBody);
            return Execute<T>(request);
        }
        public T Delete<T>(NomezeiroModel m) where T : new()
        {
            var request = new RestRequest(Method.DELETE);
            request.RequestFormat = DataFormat.Json;
            request.Resource = m.Resource;
            request.Parameters.Clear();
            request.AddJsonBody(m.JsonBody);
            return Execute<T>(request);
        }
    }
}
