﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomezeiro_2.Model
{
    public class ModelProperties
    {
        public string Resource { get; set; }
        public object JsonBody { get; set; }

        public ModelProperties SetProperties(NomezeiroModel t)
        {
            this.Resource = t.Resource;
            this.JsonBody = t.JsonBody;
            return this;
        }
    }

    public abstract class NomezeiroModel
    {
        public abstract string Resource { get; }
        public abstract object JsonBody { get; }
    }
}
