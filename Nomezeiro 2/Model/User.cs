﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Nomezeiro_2.Model
{
    public class TokenUser
    {
        public string Token { get; set; }
        public User User { get; set; }
    }
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public int Timestamp { get; set; }
    }
    public class ErrorResponse
    {
        public string Error { get; set; }
        public int Code { get; set; }
    }
}
