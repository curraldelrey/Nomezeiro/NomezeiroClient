﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomezeiro_2.Model
{
    public class FirstnameModel : NomezeiroModel
    {
        public int FirstnameID { get; set; }
        public string Firstname { get; set; }
        public int UserID { get; set; }
        public override string Resource { get { return "firstnames"; } }
        public override object JsonBody
        {
            get
            {
                return new { firstname_id = FirstnameID, firstname = Firstname, user_id = UserID };
            }
        }
    }
    public class FirstnamesModel : NomezeiroModel
    {
        public List<FirstnameModel> Firstnames { get; set; }
        public override string Resource { get { return "firstnames"; } }
        public override object JsonBody
        {
            get
            {
                return new { };
            }
        }
    }
}
