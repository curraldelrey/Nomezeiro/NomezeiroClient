﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Nomezeiro_2.Main
{
    /// <summary>
    /// Lógica interna para ViewMain.xaml
    /// </summary>
    public partial class ViewMain : Window
    {
        public ViewMain()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Closed -= Window_Closed;
            Application.Current.Shutdown();
        }
    }
}
