﻿using Nomezeiro_2.API;
using Nomezeiro_2.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Nomezeiro_2.Controls;
using MaterialDesignThemes.Wpf;
using Nomezeiro_2.Main;
using System.Windows;

namespace Nomezeiro_2.Login
{
    class ViewModelLogin : ViewModelBase
    {
        private bool isLogging, isLogged, showMessage;
        private string username, password, dialogMessage;

        public bool IsLogging
        {
            get => isLogging;
            set
            {
                isLogging = value;
                RaisePropertyChanged(nameof(IsLogging));
            }
        }
        public bool IsLogged
        {
            get => isLogged;
            set
            {
                isLogged = value;
                RaisePropertyChanged(nameof(IsLogged));
            }
        }
        public bool ShowMessage
        {
            get => showMessage;
            set
            {
                showMessage = value;
                RaisePropertyChanged(nameof(ShowMessage));
            }
        }

        public string Username { get => username;
            set
            {
                username = value;
                RaisePropertyChanged(nameof(Username));
            }
        }
        public string Password
        {
            private get => password;
            set
            {
                password = value;
                RaisePropertyChanged(nameof(Password));
            }
        }
        public string DialogMessage
        {
            private get => dialogMessage;
            set
            {
                dialogMessage = value;
                RaisePropertyChanged(nameof(DialogMessage));
            }
        }

        private ICommand loginCommand;
        public ICommand LoginCommand
        {
            get => loginCommand;
            set
            {
                loginCommand = value;
                RaisePropertyChanged(nameof(LoginCommand));
            }
        }

        public ViewModelLogin()
        {
            username = string.Empty;
            password = string.Empty;
            LoginCommand = new RelayCommand(async (obj) => await LoginAsync(obj));
            dialogMessage = "qqqqq";
        }

        private async Task LoginAsync(object obj)
        {
            IsLogging = true;
            Task<bool> initializeTask = NomezeiroAPI.Instance.Initialize(Username, Password);
            bool res = await initializeTask;
            if (res)
            {
                IsLogged = true;
                IsLogging = false;
                ViewMain mainWindow = new ViewMain();
                Application.Current.MainWindow = mainWindow;

                var lw = Application.Current.Windows[0];
                lw.Close();

                mainWindow.Show();
            }
            else
            {
                ShowMessage = true;
                //var view = new DialogMessageControl
                //{
                //    DataContext = new DialogMessageViewModel(DialogMessage)
                //};
                //var result = await DialogHost.Show(view,"RootDialog");
                //Error
            }
        }
    }
}
