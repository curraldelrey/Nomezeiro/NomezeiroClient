﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Nomezeiro_2.Login
{
    /// <summary>
    /// Lógica interna para ViewLogin.xaml
    /// </summary>
    public partial class ViewLogin : Window
    {
        public ViewLogin()
        {
            this.DataContext = new ViewModelLogin();
            InitializeComponent();
        }

        private void ViewLogin_Closed(object sender, EventArgs e)
        {
            this.Closed -= ViewLogin_Closed;
        }

        private void exit_btn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }
        private void ColorZone_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.DragMove();
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((ViewModelLogin)this.DataContext).Password = ((PasswordBox)sender).Password; }
        }
    }
}
